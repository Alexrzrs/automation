/* eslint-disable no-undef */
if (navigator.serviceWorker) {
    navigator.serviceWorker.register("/sw.js");
}

// if (window.caches) {
//     caches.open("prueba-1");
//     caches.open("prueba-2");
//     caches.has("prueba-3").then(console.log);

//     caches.delete("prueba-1").then(console.log);

//     caches
//         .open("cache-v1.1")
//         .then((cache) => {
//             cache.add("/index.html");
//             cache.addAll(["/index.html", "/favicon.ico"]);
//         })
//         .then(() => {
//             cache.delete("/favicon.ico");

//             // Todo 8. Remplazar elementos cache
//             cache.put("index.html", new Response("Hola mundo"));

//             // Todo 7. Leer cahce
//             cache.match("/index.html").then((res) => {
//                 res.text().then(console.log);
//             });
//             //Todo 9. Obtener todos los caches
//             caches.keys().then((keys) => {
//                 console.log(keys);
//             });
//         });
// }
