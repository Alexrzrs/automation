/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
importScripts(
    "https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js",
    "https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js"
);

const firebaseConfig = {
    apiKey: "AIzaSyA4xkBMkTeobVpA3o_94blzh-4TuJkVfn8",
    authDomain: "test-push-c3975.firebaseapp.com",
    projectId: "test-push-c3975",
    storageBucket: "test-push-c3975.appspot.com",
    messagingSenderId: "917420132433",
    appId: "1:917420132433:web:124c3a75a8096969f3d070",
    measurementId: "G-ZTP54FTYFQ",
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage((payload) => {
    console.log("Recibiendo msg en segundo plano");
    const tituloNotificacion = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: payload.notification.icon,
    };
    self.registration.showNotification(tituloNotificacion, options);
});
