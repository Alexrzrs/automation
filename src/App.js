import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import React from "react";
import Login from "./components/Login";
import NoteForm from "./components/NoteForm/NoteForm";

function App() {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="/notes" element={<NoteForm />} />
                {/* Add more routes as needed */}
            </Routes>
        </Router>
    );
}

export default App;
