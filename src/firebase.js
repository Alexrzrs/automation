import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getMessaging, getToken } from "firebase/messaging";

const firebaseConfig = {
    apiKey: "AIzaSyA4xkBMkTeobVpA3o_94blzh-4TuJkVfn8",
    authDomain: "test-push-c3975.firebaseapp.com",
    projectId: "test-push-c3975",
    storageBucket: "test-push-c3975.appspot.com",
    messagingSenderId: "917420132433",
    appId: "1:917420132433:web:124c3a75a8096969f3d070",
    measurementId: "G-ZTP54FTYFQ",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const messaging = getMessaging(app);
