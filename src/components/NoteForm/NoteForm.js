import React, { useState, useEffect } from "react";
import axios from "axios";
import PouchDB from "pouchdb";
import { getToken, onMessage } from "firebase/messaging";
import { messaging } from "../../firebase";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Form, Input, Button } from "antd";
import "../../App.css";

const db = new PouchDB("notes");

const NoteForm = () => {
    const getTokenNotification = async () => {
        const token = await getToken(messaging, {
            vapidKey:
                "BP3KmX_ySzAIdRu6nGZaCdG1xcir-MHyCNesV5MjSQykypmfDnqvn2BvrX_z7UbT7vkd9nk4eKKQhMOrxsEPeT8",
        }).catch((err) => console.log("No se pudo encontrar el token", err));

        if (token) {
            console.log("Token: ", token);
        }

        if (!token) {
            console.log("No hay token disponible");
        }
    };

    const notificarme = () => {
        if (!window.Notification) {
            console.log("Este navegador no soporta notificaciones");
            return;
        }

        if (Notification.permission === "granted") {
            getTokenNotification();
        } else if (
            Notification.permission !== "denied" ||
            Notification.permission === "default"
        ) {
            Notification.requestPermission().then((permission) => {
                console.log(permission);
                if (permission === "granted") {
                    getTokenNotification();
                }
            });
        }
    };

    useEffect(() => {
        notificarme();
    }, []);

    useEffect(() => {
        getTokenNotification();
        onMessage(messaging, (message) => {
            toast.info(message.notification.title);
        });
    }, []);

    const [notas, setNotas] = useState([]);
    const [titulo, setTitulo] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const [ids, setIds] = useState([]);

    useEffect(() => {
        if (ids.length > 0) {
            const query = ids.map((id) => `id=${id}`).join("&");
            const url = `https://api.restful-api.dev/objects?${query}`;
            axios
                .get(url)
                .then((res) => {
                    console.log("GET Response:", res.data);
                    setNotas(res.data);
                })
                .catch((err) => {
                    console.log("Error in GET request:", err);
                });
        }
    }, [ids]);

    useEffect(() => {
        const savedNotes = JSON.parse(localStorage.getItem("notes"));
        if (savedNotes) {
            console.log("Loaded notes from localStorage:", savedNotes);
            setNotas(savedNotes);
        }
    }, []);

    const guardar = () => {
        const note = { name: titulo, data: descripcion };
        if (navigator.onLine) {
            axios
                .post("https://api.restful-api.dev/objects", note)
                .then((res) => {
                    console.log("POST Response:", res.data);
                    const newId = res.data.id;
                    if (newId) {
                        setIds((prevIds) => [...prevIds, newId]);
                    }
                    setTitulo("");
                    setDescripcion("");
                    const updatedNotes = [...notas, res.data];
                    localStorage.setItem("notes", JSON.stringify(updatedNotes));
                    setNotas(updatedNotes);
                    console.log("Updated Notes:", updatedNotes);
                })
                .catch((err) => {
                    console.log("Error in POST request:", err);
                });
        } else {
            db.post(note)
                .then(() => {
                    const updatedNotes = [...notas, note];
                    localStorage.setItem("notes", JSON.stringify(updatedNotes));
                    setNotas(updatedNotes);
                    console.log("Saved note offline:", note);
                })
                .catch((err) => {
                    console.error("PouchDB Error:", err);
                });
        }
    };

    const handleTituloChange = (event) => {
        setTitulo(event.target.value);
    };

    const handleDescripcionChange = (event) => {
        setDescripcion(event.target.value);
    };

    return (
        <div className="container">
            <ToastContainer />
            <div className="App">
                <header className="App-header">
                    <h1>Notas con Cache</h1>
                    <label>Titulo</label>
                    <input
                        type="text"
                        value={titulo}
                        onChange={handleTituloChange}
                    />
                    <label>Datos</label>
                    <input
                        type="text"
                        value={descripcion}
                        onChange={handleDescripcionChange}
                    />
                    <button onClick={guardar}>Guardar</button>
                    <div>
                        {notas.length === 0 ? (
                            <p>No notes to display.</p>
                        ) : (
                            notas.map((nota, index) => (
                                <div
                                    key={nota.id || index}
                                    className="note-box"
                                >
                                    <h2>{nota.name}</h2>
                                    <div className="note-description">
                                        {JSON.stringify(nota.data)}
                                    </div>
                                </div>
                            ))
                        )}
                    </div>
                </header>
            </div>
        </div>
    );
};

export default NoteForm;
