import { getAuth, signInAnonymously } from "firebase/auth";
import { Link } from "react-router-dom";
import { Button } from "antd";
import loginImage from "../Assets/portada.jpg";
import "./Login.css";
import React, { useEffect } from "react";

const Login = () => {
    const login = () => {
        signInAnonymously(getAuth()).then((usuario) => console.log(usuario));
    };

    return (
        <div className="login-container">
            <img src={loginImage} className="login-image" />
            <h1>Login</h1>
            <Button type="primary" onClick={login}>
                <Link to="/notes">Login</Link>
            </Button>
        </div>
    );
};

export default Login;
